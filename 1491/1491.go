package average

func maxIndex(nums []int) (max int) {
	max = nums[0]
	for i := 0; i < len(nums); i++ {
		if nums[i] > max {
			max = nums[i]
		}
	}
	return
}

func minIndex(nums []int) (min int) {
	min = nums[0]
	for i := 0; i < len(nums); i++ {
		if nums[i] < min {
			min = nums[i]
		}
	}
	return
}

func average(salary []int) (avg float64) {
	n,max,min:= float64(len(salary)), float64(maxIndex(salary)), float64(minIndex(salary))
	for i := 0; i < int(n); i++ {
		avg += float64(salary[i])
	}
	return( avg - max -min )/ (n-2)
}
