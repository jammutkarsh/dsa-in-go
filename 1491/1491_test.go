package average

import "testing"

type addTest struct {
        salary      []int
        expected float64
}

var addTests = []addTest{
        {[]int{4000,3000,1000,2000},2500.00000},
        {[]int{1000,2000,3000}, 2000.00000},
        {[]int{1000,4000,3000, 5000}, 3500.00000},
}

func TestAverage(t *testing.T) {
	for _,test:=range addTests{
		if output:=average(test.salary); output!=test.expected {
			t.Errorf("Output: %f \t Expected: %f", output, test.expected)
		}
	}
}