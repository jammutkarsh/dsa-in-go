package maxsubarray

import "math"

// Kadande's Algorithm
func maxSubArray(nums []int) int {
	mx := math.MinInt32
	sum := 0
	for _, v := range nums {
		sum += v
		mx = max(mx, sum)
		sum = max(sum, 0)
	}
	return mx
}
