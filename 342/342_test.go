package ispoweroffour

import "testing"

func Test_isPowerOfFour(t *testing.T) {
	type args struct {
		n int
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{"1", args{1}, true},
		{"2", args{2}, false},
		{"4", args{4}, true},
		{"6", args{6}, false},
		{"11", args{11}, false},
		{"12", args{12}, false},
		{"16", args{16}, true},
		{"32", args{32}, false},
		{"64", args{64}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isPowerOfFour(tt.args.n); got != tt.want {
				t.Errorf("isPowerOfFour() = %v, want %v", got, tt.want)
			}
		})
	}
}
