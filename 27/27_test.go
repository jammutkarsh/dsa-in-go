package removeelement

import "testing"

func Test_removeElement(t *testing.T) {
	type args struct {
		nums []int
		val  int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "Example 1",
			args: args{nums: []int{3, 2, 2, 3}, val: 3},
			want: []int{2, 2},
		},
		{
			name: "Example 2",
			args: args{nums: []int{0, 1, 2, 2, 3, 0, 4, 2}, val: 2},
			want: []int{0, 1, 3, 0, 4},
		},
		{
			name: "Example 3",
			args: args{nums: []int{1}, val: 1},
			want: []int{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := removeElement(tt.args.nums, tt.args.val)
			if got != len(tt.want) {
				t.Errorf("removeElement() = %v, want %v", got, tt.want)
			}
			for i := 0; i < got; i++ {
				if tt.args.nums[i] != tt.want[i] {
					t.Errorf("removeElement() = %v, want %v", tt.args.nums[i], tt.want[i])
				}
			}
		})
	}
}
