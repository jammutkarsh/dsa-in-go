package differenceofsum

func sumOfDigits(num int) (sum int) {
	for num > 0 {
		sum += num % 10
		num = num / 10
	}
	return
}

func abs(number int) int {
	if number < 0 {
		return -1 * number
	}
	return number
}

func differenceOfSum(nums []int) int {
	arrSum, digitSum := 0, 0
	for i := 0; i < len(nums); i++ {
		arrSum += nums[i]
		digitSum += sumOfDigits(nums[i])
	}
	return abs(arrSum - digitSum)
}
