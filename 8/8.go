package myatio

import (
	"math"
	"strings"
	"unicode"
)

func myAtoi(s string) (num int) {
	s = strings.TrimSpace(s)
	strLen := len(s)
	if strLen == 0 {
		return 0
	}
	multiplier := 1
	if s[0] == '+' {
		multiplier = 1
		s = s[1:]
	} else if s[0] == '-' {
		multiplier = -1
		s = s[1:]
	}
	for _, c := range s {
		if !unicode.IsDigit(c) {
			return
		}
		digit := int(c - '0')
		//  Check 10's place and above | Edge Case: 1's places
		if num > math.MaxInt32/10 || (num == math.MaxInt32/10 && digit >= 7) {
			return math.MaxInt32
		}
		//  Check 10's place and above | Edge Case: 1's places
		if num < math.MinInt32/10 || (num == math.MinInt32/10 && digit >= 8) {
			return math.MinInt32
		}
		num = num*10 + digit*multiplier
	}
	return
}
