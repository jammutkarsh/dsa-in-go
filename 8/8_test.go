package myatio

import "testing"

func Test_myAtoi(t *testing.T) {
	tests := []struct {
		args    string
		wantRes int
	}{
		{"      -2147483667         ", -2147483648},
		{"", 0},
		{"- 4193 with words", 0},
		{"42", 42},
		{"+42", 42},
		{"2147483648", 2147483647},
	}
	for _, tt := range tests {
		t.Run("", func(t *testing.T) {
			if gotRes := myAtoi(tt.args); gotRes != tt.wantRes {
				t.Errorf("myAtoi() = %v, want %v", gotRes, tt.wantRes)
			}
		})
	}
}
