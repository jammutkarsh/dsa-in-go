package leftrightdifference

func leftRightDifference(nums []int) (result []int) {
	var length = len(nums)
	leftSum, rightSum := make([]int, length), make([]int, length)
	if length == 1 {
		return []int{0}
	}
	for i := 0; i < length; i++ {
		if i == 0 {
			leftSum[i] = 0
		} else {
			leftSum[i] = leftSum[i-1] + nums[i-1]
		}
	}
	for i := length - 1; i >= 0; i-- {
		if i == length-1 {
			rightSum[i] = 0
		} else {
			rightSum[i] = rightSum[i+1] + nums[i+1]
		}
	}
	for i := 0; i < length; i++ {
		result = append(result, leftSum[i]-rightSum[i])
		if result[i] < 0 {
			result[i] *= -1
		}
	}
	return
}
