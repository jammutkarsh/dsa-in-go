package leftrightdifference

import (
	"reflect"
	"testing"
)

func Test_leftRightDifference(t *testing.T) {
	type args struct {
		nums []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{"Leetcode example 1", args{[]int{10,4,8,3}}, []int{15,1,11,22}},
		{"Leetcode example 2", args{[]int{1}}, []int{0}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := leftRightDifference(tt.args.nums); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("leftRightDifference() = %v, want %v", got, tt.want)
			}
		})
	}
}
