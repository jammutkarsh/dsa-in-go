package subsets

import "sort"

func subsets(nums []int) [][]int {
	res = [][]int{}
	helper(0, nums)
	sort2DArray(res)
	return res
}

var res [][]int
var ds []int

func helper(index int, arr []int) {
	if index >= len(arr) {
		res = append(res, append([]int{}, ds...))
		return
	}

	ds = append(ds, arr[index])
	helper(index+1, arr)

	ds = ds[:len(ds)-1]
	helper(index+1, arr)
}

// Not required by leetcode

func sort2DArray(arr [][]int) {
	sort.Slice(arr, func(i, j int) bool {
		if len(arr[i]) == len(arr[j]) {
			for k := 0; k < len(arr[i]); k++ {
				if arr[i][k] == arr[j][k] {
					continue
				}
				return arr[i][k] < arr[j][k]
			}
		}
		return len(arr[i]) < len(arr[j])
	})
}
