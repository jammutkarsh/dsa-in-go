package largestperimeter

import (
	"sort"
)

func largestPerimeter(nums []int) (perimeter int) {
	if len(nums)<3 {
		return 0
	}
	sort.Ints(nums)
	for i := 0; i < len(nums)-2; i++ {
		if nums[i]+nums[i+1]>nums[i+2] {
			perimeter = nums[i]+nums[i+1]+nums[i+2]
		}
	}
	return perimeter
}
