package largestperimeter

import "testing"

type addTest struct {
	num []int
	expected int
}

var addTests = []addTest{
	{[]int{2,1,2},5},
	{[]int{2,1,2,2},6},
	{[]int{1,2,1,10},0},
	{[]int{1,2},0},
}

func TestSubtractProductAndSum(t *testing.T) {
	for _, test := range addTests{
		if output:= largestPerimeter(test.num); output!=test.expected{
			t.Errorf("Output: %d \t Expected: %d", output, test.expected)
		}
	}
}

