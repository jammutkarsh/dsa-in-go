package createtargetarray

import (
	"reflect"
	"testing"
)

func Test_createTargetArray(t *testing.T) {
	type args struct {
		nums  []int
		index []int
	}
	tests := []struct {
		name       string
		args       args
		wantResult []int
	}{
		{"Leetcode Example 1", args{[]int{0, 1, 2, 3, 4}, []int{0, 1, 2, 2, 1}}, []int{0, 4, 1, 3, 2}},
		{"Leetcode Example 2", args{[]int{1, 2, 3, 4, 0}, []int{0, 1, 2, 3, 0}}, []int{0, 1, 2, 3, 4}},
		{"Leetcode Example 2", args{[]int{1}, []int{0}}, []int{1}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotResult := createTargetArray(tt.args.nums, tt.args.index); !reflect.DeepEqual(gotResult, tt.wantResult) {
				t.Errorf("createTargetArray() = %v, want %v", gotResult, tt.wantResult)
			}
		})
	}
}

