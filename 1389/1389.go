package createtargetarray

func createTargetArray(nums []int, index []int) []int {
	result := make([]int, len(nums))
	for i := range nums {
		result[i] = -1
	}
	for i, val := range nums {
		if result[index[i]] == -1 {
			result[index[i]] = val
		} else {
			leftShift(result[:], index[i])
			result[index[i]] = val
		}
	}
	return result
}

func leftShift(arr []int, index int) {
	for i := len(arr) - 2; i >= index; i-- {
		arr[i+1] = arr[i]
	}
}
