package reverselist

type ListNode struct {
	Val  int
	Next *ListNode
}

func reverseList(head *ListNode) (reversed *ListNode) {
	if head == nil {
		return head
	}
	current :=  head
	for current !=nil {
		next := current.Next // next pointer
		current.Next = reversed // pointing to previous node
		reversed = current // shifting pointer
		current = next // shifting pointer
	}
	return
}
