package reverselist

import (
	"reflect"
	"testing"
)

func Test_reverseList(t *testing.T) {
	type args struct {
		head *ListNode
	}
	tests := []struct {
		name         string
		args         args
		wantReversed *ListNode
	}{
		{"Example 1", args{&ListNode{1, &ListNode{2, &ListNode{3, &ListNode{4, &ListNode{5, nil}}}}}}, &ListNode{5, &ListNode{4, &ListNode{3, &ListNode{2, &ListNode{1, nil}}}}}},
		{"Example 2", args{&ListNode{1, &ListNode{2, nil}}}, &ListNode{2, &ListNode{1, nil}}},
		{"Example 3", args{nil}, nil},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotReversed := reverseList(tt.args.head); !reflect.DeepEqual(gotReversed, tt.wantReversed) {
				t.Errorf("reverseList() = %v, want %v", gotReversed, tt.wantReversed)
			}
		})
	}
}
