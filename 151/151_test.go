package reversewords

import "testing"

func Test_reverseWords(t *testing.T) {
	tests := []struct {
		name string
		s    string
		want string
	}{
		// {"T1", "", ""},
		// {"T2", "sky blue", "blue sky"},
		// {"T3", "sky is blue", "blue is sky"},
		// {"T4", "a good   example", "example good a"},
		{"T5", "  hello world  ", "world hello"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := reverseWords(tt.s); got != tt.want {
				t.Errorf("reverseWords() = %v, want %v", got, tt.want)
			}
		})
	}
}
