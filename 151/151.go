package reversewords

import "strings"

func reverseWords(s string) (res string) {
	var i, j int
	s = strings.TrimSpace(s)
	for i < len(s) {
		for i < len(s) && s[i] == ' ' {
			i++
		}
		j = i + 1
		for j < len(s) && s[j] != ' ' {
			j++
		}
		if res == "" {
			res += s[i:j]
			i = j
		} else {
			res = s[i:j] + " " + res
			i = j
		}
	}
	return
}

// // continue keywords doesn't work on leetcode for some reason.
// func reverseWords(s string) (res string) {
// 	temp := ""
// 	for i, c := range s {
// 		if c != ' ' {
// 			temp += string(c)
// 			continue
// 		}else if s[i-1] != ' ' {
// 			res = temp + " " + res
// 			temp = ""
// 			continue
// 		}
// 	}
// 	res = temp + " " + res
// 	res = strings.TrimSpace(res)
// 	return
// }
