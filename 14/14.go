package longestcommonprefix

import "sort"

func longestCommonPrefix(strs []string) string {
	var res string
	sort.Strings(strs)
	if len(strs) == 0 || len(strs[0]) == 0 {
		return ""
	}
	for i := 0; i < len(strs[0]); i++ {
		temp := strs[0][i]
		for j := 0; j < len(strs); j++ {
			if strs[j][i] != temp {
				return res
			}
		}
		res += string(strs[0][i])
	}
	return res
}
