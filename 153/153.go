package findmin

func findMin(arr []int) int {
    low, high := 0, len(arr)-1
    if arr[low] < arr[high] {
        return arr[low]
    }
    res := int(^uint(0) >> 1)
    for low <= high {
        mid := low + (high - low)/2
        if arr[low] <= arr[mid] { // left subarray
            res = min(res, arr[low])
            low = mid + 1
        } else { // right subarray
            res = min(res, arr[mid])
            high = mid - 1
        }
    }
    return res
}