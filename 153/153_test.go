package findmin

import "testing"

func Test_findMin(t *testing.T) {
	tests := []struct {
		args []int
		want int
	}{
		{[]int{2, 1}, 1},
		{[]int{3, 4, 5, 1, 2}, 1},
		{[]int{4, 5, 6, 7, 0, 1, 2}, 0},
		{[]int{11, 13, 15, 17}, 11},
		{[]int{2, 3, 4, 5, 1}, 1},
		{[]int{7, 3, 4, 5, 6}, 3},
	}
	for _, tt := range tests {
		t.Run("", func(t *testing.T) {
			if got := findMin(tt.args); got != tt.want {
				t.Errorf("findMin() = %v, want %v", got, tt.want)
			}
		})
	}
}
