package subsetswithdup

import "sort"

func subsetsWithDup(nums []int) (res [][]int) {
	sort.Ints(nums)
	helper(0, nums, []int{}, &res)
	return
}

func helper(index int, arr []int, ds []int, res *[][]int) {
	*res = append(*res, append([]int{}, ds...))
	for i := index; i < len(arr); i++ {
		if i > index && arr[i] == arr[i-1] {
			continue
		}
		ds = append(ds, arr[i]) // add element to array
		helper(i+1, arr, ds, res)
		ds = ds[:len(ds)-1] // remove element from array
	}
}
