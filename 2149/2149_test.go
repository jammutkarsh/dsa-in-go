package rearrangearray

import (
	"reflect"
	"testing"
)

func Test_rearrangeArray(t *testing.T) {
	tests := []struct {
		name string
		nums []int
		want []int
	}{
		{"T1", []int{3, 1, -2, -5, 2, -4}, []int{3, -2, 1, -5, 2, -4}},
		{"T2", []int{1, -1}, []int{1, -1}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := rearrangeArray(tt.nums); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("rearrangeArray() = %v, want %v", got, tt.want)
			}
		})
	}
}
