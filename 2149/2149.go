package rearrangearray

func rearrangeArray(nums []int) []int {
	var p, n int
	res := make([]int, len(nums))
	for _, v := range nums {
		if v > 0 {
			res[2*p] = v
			p++
		} else {
			res[2*n+1] = v
			n++
		}
	}
	return res
}