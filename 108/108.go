package levelorder

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func levelOrder(root *TreeNode) (res [][]int) {
	queue := [](*TreeNode){root}
	for len(queue) > 0 {
		level := []int{}
		s := len(queue)
		for i := 0; i < s; i++ {
			level = append(level, queue[0].Val)
			if queue[0].Left != nil {
				queue = append(queue, queue[0].Left)
			}
			if queue[0].Right != nil {
				queue = append(queue, queue[0].Right)
			}
			queue = queue[1:]
		}
		res = append(res, level)
	}
	return
}
