package goodPair

func max(nums []int) (max int) {
	for i := 0; i < len(nums); i++ {
		if nums[i] > max {
			max = nums[i]
		}
	}
	return
}

func numIdenticalPairs(nums []int) int {
	count := 0
	for i := 0; i < len(nums); i++ {
		for j := i; j < len(nums); j++ {
			if nums[i] == nums[j] && i < j {
				count++
			}
		}
	}
	return count
}


/*
The seconday condition i < j is meaning less below. 
Since the first occurance `num` is already paired with all the upcomming `num` 
When the 2nd occurance comes, it doens't need to pair with the first one. Since it was already paired in the 1st occurance.
The chance of 2 items being in pair with repetation are n*(n-1)/2
Better explanation of formula.
https://math.stackexchange.com/questions/2214839/exactly-how-does-the-equation-nn-1-2-determine-the-number-of-pairs-of-a-given
*/
func hashNumIdenticalPairs(nums []int) int {
	count := 0
	max := max(nums)
	hashTable := make([]int, max+1)
	
	for i := 0; i < len(nums); i++ {
		hashTable[nums[i]]++
	}

	for i := 0; i < len(hashTable); i++ {
		if hashTable[i] > 1 {
			count += hashTable[i] * (hashTable[i] - 1) / 2
		}
	}
	return count
}
