package goodPair

import "testing"

type addTest struct {
	arr      []int
	expected int
}

var addTests = []addTest{
	{[]int{1, 2, 3, 1, 1, 3}, 4},
	{[]int{1, 1, 1, 1}, 6},
	{[]int{1, 2, 3}, 0},
}

func TestNumIdenticalPairs(t *testing.T) {
	for _, test := range addTests {
		if output := numIdenticalPairs(test.arr); output != test.expected {
			t.Errorf("Output: %d \t Expected: %d", output, test.expected)
		}
	}
}

func TestHashNumIdenticalPairs(t *testing.T) {
	for _, test := range addTests {
		if output := hashNumIdenticalPairs(test.arr); output != test.expected {
			t.Errorf("Output: %d \t Expected: %d", output, test.expected)
		}
	}
}