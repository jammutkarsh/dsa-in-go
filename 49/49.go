package groupanagrams

import (
	"sort"
	"strings"
)

func SortString(w string) string {
    s := strings.Split(w, "")
    sort.Strings(s)
    return strings.Join(s, "")
}

func groupAnagrams(strs []string) (res [][]string) {
    m := make(map[string][]int) // anagram -> index mapping
    for i := range strs {
        anagrm := SortString(strs[i])
        m[anagrm] = append(m[anagrm], i)
    }
    for _,idxs := range m {
        var temp []string
        for _,v := range idxs {
            temp = append(temp, strs[v])
        }
        res = append(res, temp)
    } 
    return
}