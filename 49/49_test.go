package groupanagrams

import (
	"reflect"
	"sort"
	"testing"
)

func Test_groupAnagrams(t *testing.T) {
	tests := []struct {
		name    string
		strs    []string
		wantRes [][]string
	}{
		{
			name:    "Example 1",
			strs:    []string{"eat", "tea", "tan", "ate", "nat", "bat"},
			wantRes: [][]string{{"ate", "eat", "tea"}, {"nat", "tan"}, {"bat"}},
		},
		{
			name:    "Example 2",
			strs:    []string{""},
			wantRes: [][]string{{""}},
		},
		{
			name:    "Example 3",
			strs:    []string{"a"},
			wantRes: [][]string{{"a"}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotRes := groupAnagrams(tt.strs)
			for _, v := range gotRes {
				sort.Strings(v)
			}
			for _, v := range tt.wantRes {
				sort.Strings(v)
			}
			sort.Slice(gotRes, func(i, j int) bool {
				return gotRes[i][0] < gotRes[j][0]
			})
			sort.Slice(tt.wantRes, func(i, j int) bool {
				return tt.wantRes[i][0] < tt.wantRes[j][0]
			})
			if !reflect.DeepEqual(gotRes, tt.wantRes) {
				t.Errorf("groupAnagrams() = %v, want %v", gotRes, tt.wantRes)
			}
		})
	}
}
