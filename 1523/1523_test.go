package countOdds

import "testing"

type addTest struct {
	low      int
	high     int
	expected int
}

var addTests = []addTest{
	{3, 7, 3},
	{8,10,1},
	{0,5,3},
	{0,8,4},
	{100, 101, 1},
}

func TestCountOdds(t *testing.T) {
	for _, test := range addTests {
		if output := countOdds(test.low, test.high); output != test.expected {
			t.Errorf("Output: %d \t Expected: %d", output, test.expected)
		}
	}
}
