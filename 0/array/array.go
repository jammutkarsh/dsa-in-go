package array

func Display(arr []int) {
	for i := 0; i < len(arr); i++ {
		print(arr[i])
		print(" ")
	}
	println()
}

func Append(value int, arr []int) []int {
	length := len(arr)
	arr = append(arr, 0)
	arr[length] = value
	return arr
}

func Insert(index int, value int, arr []int) []int {
	length := len(arr)
	if index > length {
		return arr
	}
	// The for loop will cause an overflow. To avoid this, we need to use the
	// built-in  append() func, because we have not defined array size
	// This array size will be adapted accordingly when the object will be initilised.
	arr = append(arr, 0)
	for i := length; i > index; i-- {
		arr[i] = arr[i-1]
	}
	arr[index] = value
	return arr
}

func Delete(index int, arr []int) []int {
	length := len(arr)
	if index > length {
		return arr
	}
	for i := index; i < length-1; i++ {
		arr[i] = arr[i+1]
	}
	return arr[:length-1]
}

func LinearSearch(key int, arr []int) int {
	for i := 0; i < len(arr); i++ {
		if key == arr[i] {
			return i
			// If Linear Search is the primary seach algorithm
			// Then we can use two approches to reduce the search time
			// If the same element is searched multiple times.
			// Transpostion: Moving the element one positoin closer to starting index.
			// Move to front/head: Swap the element with arr[0].
		}
	}
	return -1
}

func BinarySearch(key int, arr []int) int {
	low, high := 0, len(arr)-1
	for low <= high {
		mid := (low + high) / 2
		switch {
		case key == arr[mid]:
			return mid
		case key > arr[mid]:
			low = mid + 1
		case key < arr[mid]:
			high = mid - 1
		}
	}
	return -1
}

func RecursiveBinarySearch(arr []int, args ...int) int {
	key, low, high := args[0], args[1], args[2]
	if low <= high {
		mid := (low + high) / 2

		if key == arr[mid] {
			return mid
		} else if key > arr[mid] {
			return RecursiveBinarySearch(arr, key, mid+1, high)
		} else if key < arr[mid] {
			return RecursiveBinarySearch(arr, key, low, mid-1)
		}
	}

	return -1
}

/*<-- Modified functions to get specefic results -->*/

func CeilingOfNumber(key int, arr []int) int {
	if arr[0]>key {
		return 0 
	}
	if arr[len(arr)-1]<key {
		return len(arr)
	}
	low, high := 0, len(arr)-1
	for low <= high {
		mid := (low + high) / 2
		switch {
		case key == arr[mid]:
			return mid
		case key > arr[mid]:
			low = mid + 1
		case key < arr[mid]:
			high = mid - 1
		}
	}
	return low

}

func FloorOfNumber(key int, arr []int) int {
	if arr[0]>key {
		return 0 
	}
	if arr[len(arr)-1]<key {
		return len(arr)-1
	}
	low, high := 0, len(arr)-1
	for low <= high {
		mid := (low + high) / 2
		switch {
		case key == arr[mid]:
			return mid
		case key > arr[mid]:
			low = mid + 1
		case key < arr[mid]:
			high = mid - 1
		}
	}
	return high
}
