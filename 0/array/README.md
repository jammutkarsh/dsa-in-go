# Array

- Collection of `similar` data elements grouped under one name.
- `Contiguous` memory allocation takes places.
- Arrays start from `0` because it takes one less operation less to calculate the address at that position.

## Tips

> For problems related to binary searching, start you analysis with 2 elements. Since at the end you will most likely be dealing with 1 or 2 elements.
> This also helps in identifying base condition quickly.
