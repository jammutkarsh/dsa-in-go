package array

import (
	"reflect"
	"testing"
)

func TestAppend(t *testing.T) {
	type args struct {
		value int
		arr   []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{"Append", args{1, []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}}, []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Append(tt.args.value, tt.args.arr); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Append() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestInsert(t *testing.T) {
	type args struct {
		index int
		value int
		arr   []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{"Starting Index", args{0, 0, []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}}, []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10}},
		{"Middle", args{6, 111, []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}}, []int{1, 2, 3, 4, 5, 6, 111, 7, 8, 9, 10}},
		{"Last Index", args{10, 111, []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}}, []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 111}},
		{"Out Of Bound", args{34, 111, []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}}, []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Insert(tt.args.index, tt.args.value, tt.args.arr); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Append() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDelete(t *testing.T) {
	type args struct {
		index int
		arr   []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{"Starting Index", args{0, []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}}, []int{2, 3, 4, 5, 6, 7, 8, 9, 10}},
		{"Middle", args{6, []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}}, []int{1, 2, 3, 4, 5, 6, 8, 9, 10}},
		{"Last Index", args{10, []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}}, []int{1, 2, 3, 4, 5, 6, 7, 8, 9}},
		{"Out Of Bound", args{34, []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}}, []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Delete(tt.args.index, tt.args.arr); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Delete() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLinearSearch(t *testing.T) {
	type args struct {
		key int
		arr []int
	}
	tests := []struct {
		name      string
		args      args
		wantIndex int
	}{
		{"Starting Index", args{1, []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}}, 0},
		{"Middle", args{6, []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}}, 5},
		{"Last Index", args{10, []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}}, 9},
		{"Not Found", args{34, []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}}, -1},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotIndex := LinearSearch(tt.args.key, tt.args.arr); gotIndex != tt.wantIndex {
				t.Errorf("LinearSearch() = %v, want %v", gotIndex, tt.wantIndex)
			}
		})
	}
}

func TestBinarySearch(t *testing.T) {
	type args struct {
		key int
		arr []int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{"Starting Index", args{1, []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}}, 0},
		{"Middle Index", args{6, []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}}, 5},
		{"Last Index", args{10, []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}}, 9},
		{"Not Found", args{34, []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}}, -1},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := BinarySearch(tt.args.key, tt.args.arr); got != tt.want {
				t.Errorf("BinarySearch() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRecursiveBinarySearch(t *testing.T) {
	type args struct {
		arr  []int
		args []int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{"Starting Index", args{[]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, []int{1, 0, 9}}, 0},
		{"Middle Index", args{[]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, []int{6, 0, 9}}, 5},
		{"Last Index", args{[]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, []int{10, 0, 9}}, 9},
		{"Not Found", args{[]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, []int{34, 0, 9}}, -1},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RecursiveBinarySearch(tt.args.arr, tt.args.args...); got != tt.want {
				t.Errorf("RecursiveBinarySearch() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCeilingOfNumber(t *testing.T) {
	type args struct {
		key int
		arr []int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{"Starting Index", args{1, []int{1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21}}, 0},
		{"Middle Index", args{6, []int{1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21}}, 3},
		{"Last Index", args{21, []int{1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21}}, 10},
		{"Beyond Last", args{34, []int{1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21}}, 11},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := CeilingOfNumber(tt.args.key, tt.args.arr); got != tt.want {
				t.Errorf("CeilingOfNumber() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFloorOfNumber(t *testing.T) {
	type args struct {
		key int
		arr []int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{"Starting Index", args{1, []int{1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21}}, 0},
		{"Middle Index", args{6, []int{1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21}}, 2},
		{"Last Index", args{21, []int{1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21}}, 10},
		{"Beyond Last", args{34, []int{1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21}}, 10},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FloorOfNumber(tt.args.key, tt.args.arr); got != tt.want {
				t.Errorf("FloorOfNumber() = %v, want %v", got, tt.want)
			}
		})
	}
}
