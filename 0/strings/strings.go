package main

import (
	"fmt"
)

func Length(s string) int {
	n := 0
	// for _, r := range s {
	// 	n += utf8.RuneLen(r)
	// 	fmt.Println(r, n)
	// }

	key := []byte{'k', 'e', 'y'}
	m := map[string]string{}
	// The string(key) conversion copys the bytes in key.
	m[string(key)] = "value"
	// Here, this string(key) conversion doesn't copy
	// the bytes in key. The optimization will be still
	// made, even if key is a package-level variable.
	fmt.Println(m[string(key)]) // value (very possible)
	return n

}
