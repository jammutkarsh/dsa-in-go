# Strings

- **ASCII Sets**: Stands for *American Standard Code for Information Interchange* Since computers work in binary(doesn't support characters natively). We map each character with a number.
  - This is only limited to English characters and some special characters.
  - There are codes for pressing `enter`, `spacebar`, `esc`, etc.
  - Basically we map out the whole keyboard in ASCII.
  - 2^7 bits are required to represent.
- **Unicode** represents ASCII set and all the other languages like Hindi, Chinese and characters like special character or emojis.
  - Unicode extends ASCII set. Therefore, the `map[Character]CharacterCode` is same for both of them.

----

- In C/C++, the end of a character array is marked with `\0`, called as null character.
- `\0` + `Charachter Array` = `String`

Example `char name[5] string = {'a', 'b', 'c', 'd'}` is character array. While `char name [] string = {'a', 'b', 'c', 'd', '0\'}` is string `abcd`.
