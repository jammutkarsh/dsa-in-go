package main

import (
	"fmt"
)

type Node struct {
	Data int
	Next *Node
}

type LinkedList struct {
	Head   *Node
	Length int
}

// Latest element is the head of the list
//
// # NULL
//
// A -> NULL
//
// B -> A -> NULL
//
// C -> B -> A -> NULL
func (l *LinkedList) AddAtHead(n *Node) {
	temporary := l.Head
	l.Head = n
	l.Head.Next = temporary
	l.Length++
}

// Latest element is the tail of the list
//
// -> NULL
//
// A -> NULL
//
// A -> B -> NULL
//
// A -> B -> C -> NULL
func (l *LinkedList) AddAtTail(n *Node) {
	if l.IsEmptyList() {
		l.Head = n
		l.Length++
		return
	}
	current := l.Head
	for ; current.Next != nil; current = current.Next {
	}
	current.Next = n
	l.Length++
}

// element is inserted somewhere in the middle of the list
//
// A -> B -> C -> NULL
//
// A -> B -> K -> C -> NULL
func (l *LinkedList) AddAtIndex(index int, n *Node) error {
	if l.IsEmptyList() && index != 0 {
		return fmt.Errorf("Can't insert in an empty list at %d", index)
	}

	if index < 0 {
		return fmt.Errorf("Index must be non-negative")
	}

	current := l.Head
	for i := 0; i < index-1; i++ {
		if current == nil {
			return fmt.Errorf("Index out of bounds")
		}
		current = current.Next
	}

	n.Next = current.Next
	current.Next = n
	return nil
}

func (l LinkedList) IsEmptyList() bool {
	if l.Head == nil {
		return true
	} else {
		return false
	}
}

func (l LinkedList) PrintList() error {
	if l.IsEmptyList() {
		return fmt.Errorf("Can't print empty list")
	}
	current := l.Head

	for ; current != nil; current = current.Next {
		fmt.Println(current.Data)
	}
	return nil
}

// delete a node by a value
func (l *LinkedList) DeleteNodeByValue(v int) error {
	if l.IsEmptyList() {
		return fmt.Errorf("Can't delete from empty list")
	}

	if l.Head.Data == v {
		l.Head = l.Head.Next //making the 2nd element the head.
		l.Length--
		return nil
	}

	prevToDelete := l.Head
	for ; prevToDelete.Next.Data != v; prevToDelete = prevToDelete.Next {
		if prevToDelete.Next.Next == nil { // reached the end of list and haven't found the data
			return fmt.Errorf("%d doesn't exist", v)
		}
	}

	// .... J -> K -> L ....
	// we are on 'J' and pointing it's next to 'L'
	prevToDelete.Next = prevToDelete.Next.Next
	l.Length--
	return nil
}

// delete a node by a index
func (l *LinkedList) DeleteNodeByIndex(v int) error {
	if l.IsEmptyList() {
		return fmt.Errorf("Can't delete from empty list")
	}

	if l.Head.Data == v {
		l.Head = l.Head.Next //making the 2nd element the head.
		l.Length--
		return nil
	}

	prevToDelete := l.Head
	for ; prevToDelete.Next.Data != v; prevToDelete = prevToDelete.Next {
		if prevToDelete.Next.Next == nil { // reached the end of list and haven't found the data
			return fmt.Errorf("%d doesn't exist", v)
		}
	}

	// .... J -> K -> L ....
	// we are on 'J' and pointing it's next to 'L'
	prevToDelete.Next = prevToDelete.Next.Next
	l.Length--
	return nil
}

func ZippingList(list1, list2 *LinkedList) *LinkedList {
	tail := list1.Head
	current1 := list1.Head.Next
	current2 := list2.Head
	count := 0
	for current1 != nil && current2 != nil {
		if count%2 == 0 {
			tail.Next = current2
			current2 = current2.Next
		} else {
			tail.Next = current1
			current1 = current1.Next
		}
		tail = tail.Next
		count++
	}

	if current1 != nil {
		tail.Next = current1
	}

	if current2 != nil {
		tail.Next = current2
	}

	list1.Length += list2.Length
	return list1
}

func main() {
	// List 1
	myList := LinkedList{}

	node1 := &Node{Data: 1}
	myList.AddAtTail(node1)

	node2 := &Node{Data: 2}
	myList.AddAtTail(node2)

	node3 := &Node{Data: 3}
	myList.AddAtTail(node3)

	node4 := &Node{Data: 4}
	myList.AddAtTail(node4)

	node5 := &Node{Data: 5}
	myList.AddAtTail(node5)

	// List 2
	myList2 := LinkedList{}

	node12 := &Node{Data: 12}
	myList2.AddAtTail(node12)

	node22 := &Node{Data: 22}
	myList2.AddAtTail(node22)

	node32 := &Node{Data: 32}
	myList2.AddAtTail(node32)

	node42 := &Node{Data: 42}
	myList2.AddAtTail(node42)

	node52 := &Node{Data: 52}
	myList2.AddAtTail(node52)

	ZippingList(&myList, &myList2).PrintList()
}
