# Recursion

- Function `calling itself`
- Traces a `tree` like structure.
- `Changes in order` of function call `makes difference`.

## Changing In Order

*NOTE: The flow/order of diagram is top to bottom and left to right*

- **Ascending**: statements executed at *calling* time.

```go
func Recur(n int){
 if n>0 {
  print(n)
  Recurr(n-1)
 }
} 
// Output: 3 2 1
```

```mermaid  
graph TD
Main --> Recur3
Recur3--> printf3
Recur3--> Recur2
Recur2--> printf2
Recur2--> Recur1
Recur1--> printf1
Recur1--> Recur0
Recur0--> X
```

- **Descending**: statements executed at *returning* time.

```go
func Recur(n int){
 if n>0 {
  Recurr(n-1)
  print(n)
 }
} 
// Output: 3 2 1
```

```mermaid  
graph TD
Main --> Recur3
Recur3--> Recur2
Recur3--> printf3
Recur2--> Recur1
Recur2--> printf2
Recur1--> Recur0
Recur0--> X
Recur1--> printf1
```

### Tracing Tree

- Uses `stack` portion of memory to allocate variables.
- Follows `FILO` approach.
- One stack added per function call.
- Each stack retains its value used in the `Func` like `var`.

```go
<-- HEAP Section -->
--------------------
<-- STACK Section --> // For ascending
Func(n) // 'n' calls made
...
...
Func(2)
Func(1)
Main()
--------------------
<-- CODE Section -->
code of main and func is here
```

### Variables

- *Static Variables* and *global variable* act in same way in recursion.

### Types

#### 1. Tail Recursion

**Space Complexity:** O(n)
**Time Complexity:** O(n)

- If the recursive call is last part of the function.
- **Everything** is done at **calling** time
- **No** operation at **returning** time.

```go
func rec(n int){
 if(n>0){
  printf(n)
  rec(n-1) // Nothing below, Anything Above
 } // If any operation is done on `rec(n-1)` then it won't be called as tail recursion.
}
// Loop Version
func rec(n int){
 for n>0 {
  printf(n)
  n--
 }
}
```

#### 2. Head Recursion

**Space Complexity:** O(n)
**Time Complexity:** O(n)

- If the recursive call is first part of the function.
- **Everything** is done at **returning** time
- **No** operation at **calling** time.

```go
func rec(n int){
 if(n>0){
  rec(n-1) // Nothing Above, Anything below
  printf(n)
 }
}
```

- Not every head recursion can be converted into a loop directly.

>[!info]
>**Efficiency:** Loop > Tail Or Head Recursion
>
#### 3. Tree Recursion

- If the recursive function is called **more than one** time.

```go
func rec(n int){
 if(n>0){
  printf(n)
  rec(n-1)
  rec(n-1)
 }
}
```

```mermaid
graph TD

A[Recur 3]--> B[Print 3] & C[Recur 2] & D[Recur 2]

C --> E[Print 2] & F[Recur 1] & G[Recur 1]

F --> H[Print 1] & I[Recur 0]

I --> 1[X]

G --> J[Print 1] & K[Recur 0]

K --> 2[X]

D --> D1[Print 2] & E1[Recur 1] & G1[Recur 1]

E1 --> E11[Print 1] & E22[Recur 0]

E22 --> 222[X]

G1 --> E111[Print 1] & E222[Recur 0]

E222 --> 2222[X]
```

- `n+1` is the height of tree.
- No. of function call made are 2^n+1</sup> - 1

#### 4. Indirect Recursion

- When two functions call each other in a cyclic manner

```go
func recA(n int){
 if(n>0){
  printf(n)
  recB(n-1)
 }
}
func recB(n int){
 if(n>1){
  printf(n)
  recA(n/2)
 }
}
```

```mermaid
graph TD
A[RecurA 20]--> B[Print 20] & C[RecurB 19]

C--> D[Print 19] & E[RecurA 9]

E--> F[Print 9] & G[RecurB 8]

G--> H[Print 8] & I[RecurA 4]

I--> J[Print 4] & K[RecurB 3]

K--> M[Print 3] & N[RecurA 1]

N--> P[Print 1] & Q[RecurB 0]

Q--> X
```

#### 5. Nested Recursion

- The parameter of the recursive call is the recursive call itself.

```go
func rec(n int){
 if(n>100){
  return n-10
 }else{
  return rec(rec(n+11))
 }
```

```mermaid
graph TD

A[Recur 95]--> B[Recur Recur 95 + 11 ]

B--> C[Recur 96]

C--> D[Recur Recur 96 + 11 ]

D--> E[Recur 97]

E--> F[Recur Recur 97 + 11 ]

F--> G[Recur 98]

G--> H[Recur Recur 98 + 11 ]

H--> I[Recur 99]

I--> J[Recur Recur 99 + 11 ]

J--> K[Recur 100]

K--> L[Recur Recur 100 + 11 ]

L--> M[Recur 101]

M--> 91
```
