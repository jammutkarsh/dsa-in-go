package recursion

func SumOfNaturalNo(number int) (sum int) {
	/* Style 1: Ascending type */
	if number > 0 {
		return number + SumOfNaturalNo(number-1)
	}
	return

	/* Style 2: Descending type*/
	// if number == 0 {
	// 	return 0
	// } else {
	// 	return SumOfNaturalNo(number-1) + number
	// }

	/*Style 3: Using Loop*/
	// for i := 1; i <= number; i++ {
	// 	sum+=i
	// }
	// return sum

	/* Style 4: Using the formula*/
	// return number*(number+1)/2
}

func Factorial(number int) (fact int) {
	/*Style 1: Descending Type*/
	if number > 1 {
		return Factorial(number-1) * number
	}
	return 1

	/*Style2: Ascending Type*/
	// if number == 1 || number == 0 {
	// 	return 1
	// } else {
	// 	return number * Factorial(number-1)
	// }

	/*Style 3: Loop*/
	// fact=1
	// for i := number; i>0;i--  {
	// 	fact*=i
	// }
	// return fact
}

func Power(num, power int) (ans int) {
	/*Style 1: Performing the multiplication till power*/
	// if power == 0 {
	// 	return 1
	// }
	// return Power(num, power-1) * num

	/*Style 2: Reduction of power*/
	if power==0{
		return 1
	}else if power%2==0{
		return Power(num*num, power/2)
	}else{
		return num*Power(num*num, (power-1)/2)
	}
}

var (
	// product       int     = 1
	// factorial     float64 = 1
	hornerProduct float64 = 1
)

func TaylorSeries(x, n int) (ans float64) {
	/*Style 1: Implementing directly by observing the formula*/
	// if n == 0 {
	// 	return 1
	// }
	// r := TaylorSeries(x, n-1)
	// product *= x
	// factorial *= float64(n)
	// return r + float64(product)/factorial

	/*Style 2: Simplifying the equation using Horner's rule. */
	/*Style 2.1: Using For Loop*/
	// ans = 1
	// for ; n>0; n-- {
	// 	ans = 1 + float64(x)/float64(n)*ans
	// }
	// return

	/*Style 2.1: Using For Recursion*/
	if n == 0 {
		return hornerProduct
	}
	hornerProduct = 1 + hornerProduct*float64(x)/float64(n)
	return TaylorSeries(x, n-1)
}

// this is an unrefferenced variable. It will be used to initialize the map.
var recursionArray map[int]int


func FibonacciSeries(n int) int {
	if n <= 1 {
		return n
	}
	/*Style 1: Using a for loop*/
	// t0, t1, sum := 0,1,0
	// for i := 2; i <=n; i++ {
	// 	sum = t0+t1
	// 	t0 = t1
	// 	t1= sum
	// }
	// return sum

	/*Style 2: Using recrusrion*/

	/*Style 2.1: Using Excessive Recursion
	Excessive Recursion: A type of recursive which calls itself multiple times for same value*/
	// return FibonacciSeries(n-1) + FibonacciSeries(n-2)

	/*Style 2.1: Using  recrusrion with a hash table array or map.*/
	if recursionArray == nil {
		recursionArray = make(map[int]int)
	}
	if _, ok := recursionArray[n]; ok {
		return recursionArray[n]
	}
	recursionArray[n] = FibonacciSeries(n-1) + FibonacciSeries(n-2)
	return recursionArray[n]
}

func Combination(n, r int) int {
	// Sanity Check, since r cannot be greater than n
	if r > n {
		return -1
	}
	// Style 1: By using nCr formula of factorial.
	// return Factorial(n)/(Factorial(r)*Factorial(n-r))

	// Style 2: Using Recursion from Pascal’s Triangle
	if r == 0 || n == r {
		return 1
	}
	return Combination(n-1, r) + Combination(n-1, r-1)
}
