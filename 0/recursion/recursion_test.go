package recursion

import (
	"math"
	"testing"
)

// withinTolerance finds wether the two values same value upto deciml value defined by expVal.
//
// a & b are the two variables that needs to be comapred.
func withinTolerance(a, b, expVal float64) bool {
	// 1 e -3 means 1 * 10^ -3
	// 4.3312 e 2 means 4.3312 * 10^ 2
	if a == b {
		return true
	    }
	    d := math.Abs(a - b)
	    if b == 0 {
		return d < expVal
	    }
	    return (d / math.Abs(b)) < expVal
	}

func TestSumOfNaturalNo(t *testing.T) {
	var tests = []struct {
		num      int
		expected int
	}{
		{0, 0 * 1 / 2},
		{1, 1 * 2 / 2},
		{10, 10 * 11 / 2},
		{100, 100 * 101 / 2},
	}
	for _, test := range tests {
		if output := SumOfNaturalNo(test.num); output != test.expected {
			t.Errorf("Expected: %d\t Output: %d", test.expected, output)
		}
	}
}

func TestFactorial(t *testing.T) {
	var tests = []struct {
		num      int
		expected int
	}{
		{0, 1},
		{1, 1},
		{5, 120},
		{10, 3628800},
	}
	for _, test := range tests {
		if output := Factorial(test.num); output != test.expected {
			t.Errorf("Expected: %d\t Output: %d", test.expected, output)
		}
	}
}

func TestPower(t *testing.T) {
	var tests = []struct {
		number   int
		exponent int
		expected int
	}{
		{1, 0, 1},
		{1, 1, 1},
		{10000, 0, 1},
		{1, 100, 1},
		{2, 10, 1024},
		{5, 4, 625},
	}
	for _, test := range tests {
		if output := Power(test.number, test.exponent); output != test.expected {
			t.Errorf("Expected: %d\t Output: %d", test.expected, output)
		}
	}
}

func TestTaylorSeries(t *testing.T) {
	tests := []struct {
		x        int
		n        int
		expected float64
	}{
		// due to absense of global variables, for now test cases must be tested as individually
		// {1, 1, 2},
		// {0, 1, 1},
		// {4, 15, 54.597885},
		{15, 4, 2800.375},
	}
	for _, tt := range tests {
		if output:= TaylorSeries(tt.x, tt.n); !withinTolerance(tt.expected, output, 1e-3) {
			t.Errorf("Expected: %.18f\t Output: %.18f", tt.expected,output )
		}
	}
}

func TestFibonacciSeries(t *testing.T) {
	var tests = []struct {
		num      int
		expected int
	}{
		{2, 1},
		{3, 2},
		{9, 34},
	}
	for _, test := range tests {
		if output := FibonacciSeries(test.num); output != test.expected {
			t.Errorf("Expected: %d\t Output: %d", test.expected, output)
		}
	}
}
func TestCombination(t *testing.T) {
	var tests = []struct {
		n int
		r int
		expected int
	}{
		{1,1,1},
		{0,0,1},
		{1,0,1},
		{15,4,1365},
		{15,16,-1},
	}
	for _, test := range tests {
		if output := Combination(test.n, test.r); output != test.expected {
			t.Errorf("Expected: %d\t Output: %d", test.expected, output)
		}
	}
}