package mostWordsFound

import "testing"

type addTest struct {
	arr      []string
	expected int
}

var addTests = []addTest{
	{[]string{"alice and bob love leetcode", "i think so too", "this is great thanks very much"}, 6},
	{[]string{"please wait", "continue to fight", "continue to win"}, 3},
}

func TestMostWordsFound(t *testing.T) {
        for _, test := range addTests {
                if output := mostWordsFound(test.arr); output != test.expected {
                        t.Errorf("Output: %d \t Expected: %d", output, test.expected)
                }
        }
}