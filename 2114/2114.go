package mostWordsFound

import "strings"

func mostWordsFound(sentences []string) (max int) {
	for i := 0; i < len(sentences); i++ {
		count := strings.Count(sentences[i], " ")
		if count>max {
			max=count
		}
	}
	return max+1
}
