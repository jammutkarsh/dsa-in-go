package valueAfterOperation

import "testing"

type addTest struct {
	arr      []string
	expected int
}

var addTests = []addTest{
	{[]string{"x++", "X++", "++X", "--X"}, 2},
	{[]string{"--X", "X++", "X++"}, 1},
	{[]string{"--X", "--X", "X++"}, -1},
}

func TestFinalValueAfterOperations(t *testing.T) {
	for _, test := range addTests {
		if output := finalValueAfterOperations(test.arr); output != test.expected {
			t.Errorf("Output: %d \t Expected: %d", output, test.expected)
		}
	}
}


