package valueAfterOperation

import "strings"

func finalValueAfterOperations(operations []string) (value int) {
	for _, sign := range operations {
		if strings.Contains(sign, "-") {
			value -= 1
		}
		if strings.Contains(sign, "+") {
			value += 1
		}
	}
	return value
}
