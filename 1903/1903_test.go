package largestoddnumber

import "testing"

func Test_largestOddNumber(t *testing.T) {
	tests := []struct {
		num string
		want string
	}{
		{"52", "5"},
		{"2468", ""},
		{"1357", "1357"},
		{"2427", "2427"},
		{"1235788", "12357"},
		{"10133890", "1013389"},
	}
	for _, tt := range tests {
		t.Run("", func(t *testing.T) {
			if got := largestOddNumber(tt.num); got != tt.want {
				t.Errorf("largestOddNumber() = %v, want %v", got, tt.want)
			}
		})
	}
}
