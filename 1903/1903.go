package largestoddnumber

func largestOddNumber(num string) string {
	n := len(num)
	if num[n-1]%2 != 0 {
		return num
	}
	for i := n - 1; i >= 0; i-- {
		if num[i]%2 != 0 {
			return num[:i+1]
		}
	}
	return ""
}
