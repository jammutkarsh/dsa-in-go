package canBeIncreasing

import "testing"

func TestCanBeIncreasing(t *testing.T) {
	var testCases = []struct {
		nums     []int
		expected bool
	}{
		{[]int{1, 2, 10, 7, 9}, true},
		{[]int{9, 8, 7, 6, 5}, false},
		{[]int{2, 3, 1, 2}, false},
		{[]int{1, 1, 1, 1, 1}, false},
		{[]int{0}, true},
	}
	for _, test := range testCases {
		if output := canBeIncreasing(test.nums); output != test.expected {
			t.Errorf("\nFor input %d\nExpected: %t \t Got: %t\n\n", test.nums, test.expected, output)
		}
	}
}
