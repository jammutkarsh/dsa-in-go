package canBeIncreasing

func canBeIncreasing(nums []int) bool {
	max, count := nums[0], 0
	if len(nums) == 1 {
		return true
	}
	for i := 1; i < len(nums) && count < 2; i++ {
		if max >=nums[i]  {
			count++
			if i > 1 && nums[i-2] >= nums[i] {
				max = nums[i-1]
				continue
			}
		}
		max = nums[i]
	}
	return count < 2
}
/*
The first if statement is checking if the no. previous to it is bigger or not
When it finds one, it steps inside adds count++
To satisfy the condition, we need 
A. count == 0 i.e. alwasys increasing
B. count == 1 i.e. only one element is bigger
If count goes beyond 2, then we exit the loop.

In the 2nd if statment, we check for i to ensure we don't access array beyond the scope, eg: arr[-1]
Then we simply check if x < z in series x, y, z where z is the no. we have to remove.
*/

/*
1, 2, 10, 7, 9
max = 10
nums[i] = 7
nums[i-2] = 2
*/