package smallerNumbersThanCurrent

import "testing"

func TestSmallerNumbersThanCurrent(t *testing.T) {
	addTests := []struct {
		num []int
		expected []int
	}{
		{[]int{8,1,2,2,3},[]int{4,0,1,1,3}},
		{[]int{6,5,4,8},[]int{2,1,0,3}},
		{[]int{7,7,7,7},[]int{0,0,0,0}},
	}
	for _, test := range addTests {
		output := smallerNumbersThanCurrent(test.num)
		for i, v := range test.expected {
			if v != output[i] {
				t.Errorf("Output: %d \t Expected: %d", output, test.expected)
				break
			}
		}
	}
}