package removeduplicates

import "testing"

func Test_removeDuplicates(t *testing.T) {
	tests := []struct {
		nums  []int
		wantK int
	}{
		{[]int{1, 1, 2}, 2},
		{[]int{0, 0, 1, 1, 1, 2, 2, 3, 3, 4}, 5},
	}
	for _, tt := range tests {
		t.Run("", func(t *testing.T) {
			if gotK := removeDuplicates(tt.nums); gotK != tt.wantK {
				t.Errorf("removeDuplicates() = %v, want %v", gotK, tt.wantK)
			}
		})
	}
}
