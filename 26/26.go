package removeduplicates

func removeDuplicates(nums []int) (k int) {
    for i := 1; i<len(nums); i++ {
        if nums[k]!= nums[i] {
            k++
            nums[k] = nums[i]
        }
    }
    return k + 1
}