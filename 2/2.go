package addtwonumbers

type ListNode struct {
	Val  int
	Next *ListNode
}

func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
	var res *ListNode = &ListNode{0, nil}
	carry, temp := 0, res
	for l1 != nil || l2 != nil {
		sum := 0
		if l1 != nil {
			sum += l1.Val
			l1 = l1.Next
		}
		if l2 != nil {
			sum += l2.Val
			l2 = l2.Next
		}
		if carry != 0 {
			sum += carry
		}
		carry = sum / 10
		temp.Next = &ListNode{sum % 10, nil}
		temp = temp.Next
	}
	if carry != 0 {
		temp.Next = &ListNode{carry, nil}
	}
	return res.Next
}
