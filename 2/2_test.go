package addtwonumbers

import (
	"reflect"
	"testing"
)

func Test_addTwoNumbers(t *testing.T) {
	type args struct {
		l1 *ListNode
		l2 *ListNode
	}
	tests := []struct {
		name    string
		args    args
		wantRes *ListNode
	}{
		// {
		// 	name: "Example 1",
		// 	args: args{
		// 		l1: &ListNode{2, &ListNode{4, &ListNode{3, nil}}},
		// 		l2: &ListNode{5, &ListNode{6, &ListNode{4, nil}}},
		// 	},
		// 	wantRes: &ListNode{7, &ListNode{0, &ListNode{8, nil}}},
		// },
		// {
		// 	name: "Example 2",
		// 	args: args{
		// 		l1: &ListNode{0, nil},
		// 		l2: &ListNode{0, nil},
		// 	},
		// 	wantRes: &ListNode{0, nil},
		// },
		{
			name: "Example 3",
			args: args{
				l1: &ListNode{9, &ListNode{9, &ListNode{9, &ListNode{9, nil}}}},
				l2: &ListNode{9, &ListNode{9, &ListNode{9, &ListNode{9, &ListNode{9, &ListNode{9, &ListNode{9, nil}}}}}}},
			},
			wantRes: &ListNode{8, &ListNode{9, &ListNode{9, &ListNode{9, &ListNode{0, &ListNode{0, &ListNode{0, &ListNode{1, nil}}}}}}}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotRes := addTwoNumbers(tt.args.l1, tt.args.l2); !reflect.DeepEqual(gotRes, tt.wantRes) {
				t.Errorf("addTwoNumbers() = %v, want %v", gotRes, tt.wantRes)
			}
		})
	}
}
