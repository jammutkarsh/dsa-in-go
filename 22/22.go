package generateparenthesis

var res []string

func generateParenthesis(n int) []string {
	res = []string{}
	helper(n, 0, 0, "")
	return res
}

func helper(n, o, c int, s string) {
	if o == n && c == n {
		res = append(res, s)
		return
	}
	if o < n {
		s += "("
		helper(n, o+1, c, s)
		s = s[:len(s)-1]
	}
	if c < o {
		s += ")"
		helper(n, o, c+1, s)
		s = s[:len(s)-1]
	}
}
