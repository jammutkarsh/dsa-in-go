package hammingweight

import "testing"

type addTest struct {
	num uint32
	expected int
}

var addTests = []addTest{
	{00000000000000000000000000001011, 3},
	{00000000000000000000000010000000,1},
}

func TestLeetCodeCopyhammingWeight(t *testing.T) {
	for _, test := range addTests{
		if output:= leetCodeCopyhammingWeight(test.num); output!=test.expected{
			t.Errorf("Output: %d \t Expected: %d", output, test.expected)
		}
	}
}
func TestNeetCodeSolutionHammingWeight1(t *testing.T) {
	for _, test := range addTests{
		if output:= neetCodeSolutionHammingWeight1(test.num); output!=test.expected{
			t.Errorf("Output: %d \t Expected: %d", output, test.expected)
		}
	}
}
func TestNeetCodeSolutionHammingWeight2(t *testing.T) {
	for _, test := range addTests{
		if output:= neetCodeSolutionHammingWeight2(test.num); output!=test.expected{
			t.Errorf("Output: %d \t Expected: %d", output, test.expected)
		}
	}
}