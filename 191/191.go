package hammingweight

func leetCodeCopyhammingWeight(num uint32) int {
	count := uint32(0)
	for ; num > uint32(0); num >>= 1 {
		count += num & 1
	}

	return int(count)
}

func neetCodeSolutionHammingWeight1(num uint32) int {
	count := 0
	// num >>= 1 --> num = num >> 1
	for ; num > 0; num >>= 1 {
		if num%2 == 1 {
			count++
		}
	}
	return count
}

func neetCodeSolutionHammingWeight2(num uint32) int {
	count := 0
	for ; num > 0; num=num&(num-1) {
		count++
	}
	return count
}
