package nearestValidPoint

import "math"

func nearestValidPoint(x int, y int, points [][]int) (index int) {
	index = -1
	distance:= math.MaxInt
	for i, point := range points {
		if point[0] == x || point[1] == y {
			if absDis := getAbsoluteDistance(point[0], point[1], x, y); distance > absDis {
				distance = absDis
				index = i
			}
		}
	}
	return index
}

func getAbsoluteDistance(x1, y1, x2, y2 int) (absDis int) {
	absDis = (x2 - x1) + (y2 - y1)
	if absDis < 0 {
		return -1 * absDis
	}
	return absDis
}
