package nearestValidPoint

import "testing"

type addTest struct {
	x int
	y int
	points [][]int
	expected int
}

var addTests = []addTest{
	{3, 4, [][]int{{1,2},{3,1},{2,4},{2,3},{4,4}}, 2},
	{3, 4, [][]int{{3,4}}, 0},
	{3, 4, [][]int{{2,3}}, -1},
}

func TestNearestValidPoint(t *testing.T) {
	for _, test := range addTests{
		if output:= nearestValidPoint(test.x, test.y, test.points); output!=test.expected{
			t.Errorf("Output: %d \t Expected: %d", output, test.expected)
		}
	}
}