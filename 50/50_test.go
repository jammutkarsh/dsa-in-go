package pow

import (
	"math"
	"testing"
)

func Test_myPow(t *testing.T) {
	type args struct {
		x float64
		n int
	}
	tests := []struct {
		args args
		want float64
	}{
		{args{1.00000, 100}, 1.00000},
		{args{2.00000, 10}, 1024.00000},
		{args{2.00000, -2}, 0.25000},
		{args{2.10000, 3}, 9.26100},
	}
	for _, tt := range tests {
		t.Run("", func(t *testing.T) {
			got := myPow(tt.args.x, tt.args.n)
			got = math.Floor(got*100000)/100000
			if got != tt.want {
				t.Errorf("myPow() = %v, want %v", got, tt.want)
			}
		})
	}
}
