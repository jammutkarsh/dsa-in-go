package pow

func myPow(x float64, n int) (ans float64) {
	if x == float64(1) || n == 0 {
		return float64(1)
	}
	var neg bool
	if n < 0 {
		neg = true
		n *= -1
	}
	ans = 1
	for n > 0 {
		if n%2 == 1 {
			ans *= x
			n--
		} else {
			x *= x
			n /= 2
		}
	}
	if neg {
		ans = 1 / ans
	}
	return
}
