package countgoodnumbers

import "testing"

func Test_countGoodNumbers(t *testing.T) {
	tests := []struct {
		name string
		n    int64
		want int
	}{
		// {"T1", 1, 5},
		// {"T2", 2, 20},
		// {"T3", 50, 564908303},
		// {"T4", 4, 400},
		{"T5", 1924, 805821919},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := countGoodNumbers(tt.n); got != tt.want {
				t.Errorf("countGoodNumbers() = %v, want %v", got, tt.want)
			}
		})
	}
}
