package countgoodnumbers

const mod int = 1_000_000_007

// Leetcode 1922
func countGoodNumbers(n int64) int {
	even, odd := (n+1)/2, n/2
	first := myPow(5, even) % mod
	second := myPow(4, odd) % mod
	return (first * second) % mod
}

func myPow(v int, n int64) int {
	if n == 0 {
		return 1
	}
	if n%2 == 1 {
		return (myPow(v, n-1) * v) % mod
	} else {
		//v*v get too big, so we need to cut that down
		return myPow(v*v%mod, n/2) % mod
	}
}
