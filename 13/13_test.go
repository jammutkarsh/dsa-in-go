package romantoint

import "testing"

func Test_romanToInt(t *testing.T) {
	tests := []struct {
		s string
		wantVal int
	}{
		{"III", 3},
		{"LVIII", 58},
		{"MCMXCIV", 1994},
	}
	for _, tt := range tests {
		t.Run("", func(t *testing.T) {
			if gotVal := romanToInt(tt.s); gotVal != tt.wantVal {
				t.Errorf("romanToInt() = %v, want %v", gotVal, tt.wantVal)
			}
		})
	}
}
