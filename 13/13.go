package romantoint

func romanToInt(s string) (val int) {
	roman := map[byte]int{'I': 1, 'V': 5, 'X': 10, 'L': 50, 'C': 100, 'D': 500, 'M': 1000}
	for i, v := range s {
		if i < len(s)-1 && roman[byte(v)] < roman[s[i+1]] {
			val -= roman[byte(v)]
		} else {
			val += roman[byte(v)]
		}
	}
	return
}
