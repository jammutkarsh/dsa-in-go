package oddevenlist

type ListNode struct {
	Val  int
	Next *ListNode
}

func oddEvenList(head *ListNode) *ListNode {
	var even, odd *ListNode = &ListNode{}, &ListNode{}
	dumEven, dumOdd := even, odd
	swch := true
	for i := head; i != nil; i = i.Next {
		if swch {
			odd.Next = i
			odd = odd.Next
		} else {
			even.Next = i
			even = even.Next
		}
		swch = !swch
	}
	even.Next = nil
	odd.Next = dumEven.Next
	return dumOdd.Next
}
