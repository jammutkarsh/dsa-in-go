package arraySign

import "sort"

func arraySign(nums []int) int {
	sort.Ints(nums)
	negCount := 0
	for i, num := range nums {
		if nums[i] == 0 {
			return 0
		} else if num > 0 {
			break
		}
		if nums[i] < 0 {
			negCount++
		}
	}

	if negCount%2 == 1 {
		return -1
	}
	return 1
}
