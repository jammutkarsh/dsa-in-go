package arraySign

import "testing"

var tests = []struct {
	arr      []int
	expected int
}{
	{[]int{-1, -2, -3, -4, 3, 2, 1}, 1},
	{[]int{1, 5, 0, 2, -3}, 0},
	{[]int{1, -1, 1, -1, -1}, -1},
	{[]int{100, 200, 300, -1, -20}, 1},
	{[]int{-5}, -1},
	{[]int{0},0},
	{[]int{1},1},
}

func TestArraySign(t *testing.T) {
	for _, test := range tests {
		if output := arraySign(test.arr); output != test.expected {
			t.Errorf("Output: %d \t Expected: %d", output, test.expected)
		}
	}
}
