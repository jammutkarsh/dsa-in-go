package removeouterparentheses

func removeOuterParentheses(s string) string {
	var count int
	var res string
	for _, c := range s {
		if c == '(' {
			if count > 0 {
				res += string(c)
			}
			count++
		}
		if c == ')' {
			count--
			if count > 0 {
				res += string(c)
			}
		}
	}
	return res
}
