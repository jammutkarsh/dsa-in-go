package defangipaddr

func defangIPaddr(address string) (newAddress string) {
	length := len(address)
	if length == 0 {
		return ""
	}

	for _, letter := range address {
		if string(letter) != "." {
			newAddress += string(letter)
		} else {
			newAddress += "[.]"
		}
	}

	return
}
