package ispalindrome

import "testing"

func Test_isPalindrome(t *testing.T) {
	tests := []struct {
		name string
		x    int
		want bool
	}{
		{
			name: "5",
			x:    5,
			want: true,
		},
		{
			name: "121",
			x:    121,
			want: true,
		},
		{
			name: "10",
			x:    10,
			want: false,
		},
		{
			name: "-121",
			x:    -121,
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isPalindrome(tt.x); got != tt.want {
				t.Errorf("isPalindrome() = %v, want %v", got, tt.want)
			}
		})
	}
}
