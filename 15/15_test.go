package threesum

import (
	"reflect"
	"testing"
)

func Test_threeSum(t *testing.T) {
	tests := []struct {
		name    string
		nums    []int
		wantRes [][]int
	}{
		{
			name:    "example 1",
			nums:    []int{-1, 0, 1, 2, -1, -4},
			wantRes: [][]int{{-1, -1, 2}, {-1, 0, 1}},
		},
		{
			name:    "example 2",
			nums:    []int{0, 0, 0},
			wantRes: [][]int{{0, 0, 0}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotRes := threeSum(tt.nums); !reflect.DeepEqual(gotRes, tt.wantRes) {
				t.Errorf("threeSum() = %v, want %v", gotRes, tt.wantRes)
			}
		})
	}
}
