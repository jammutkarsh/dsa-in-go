package threesum

import "sort"

func threeSum(nums []int) (res [][]int) {
	l := len(nums)
	sort.Ints(nums)
	for i := 0; i < l; i++ {
		if i > 0 && nums[i] == nums[i-1] {
			continue
		}
		j, k := i+1, l-1
		for j < k {
			sum := nums[i] + nums[j] + nums[k]
			if sum > 0 {
				k--
			} else if sum < 0 {
				j++
			} else {
				res = append(res, []int{nums[i], nums[j], nums[k]})
				j++
				k--
				for k > j && nums[j] == nums[j-1] {
					j++
				}
				for k > j && nums[k] == nums[k+1] {
					k--
				}
			}
		}
	}
	return
}
