package setzeroes

func setZeroes(matrix [][]int) {
	col0 := matrix[0][0]
	// FInd the zeros and flag them.
	for i := 0; i < len(matrix); i++ {
		for j := 0; j < len(matrix[0]); j++ {
			if matrix[i][j] == 0 {
				matrix[i][0] = 0
				if j != 0 {
					matrix[0][j] = 0
				} else {
					col0 = 0
				}
			}
		}
	}
	// Make all the rows and column zero, except first row and first column
	for i := len(matrix) - 1; i > 0; i-- {
		for j := len(matrix[0]) - 1; j > 0; j-- {
			if matrix[i][0] == 0 || matrix[0][j] == 0 {
				matrix[i][j] = 0
			}
		}
	}
	// Mark first row as zero
	if matrix[0][0] == 0 {
		for i := 0; i < len(matrix[0]); i++ {
			matrix[0][i] = 0
		}
	}
	// Mark first column as zero
	if col0 == 0 {
		for i := 0; i < len(matrix); i++ {
			matrix[i][0] = 0
		}
	}
}
