package search

func search(nums []int, target int) int {
	low, high := 0, len(nums)-1
	for high>=low{
		mid := (high + low) / 2
		switch {
		case target == nums[mid]:
			return mid
		case target > nums[mid]:
			low = mid + 1
		case target < nums[mid]:
			high = mid - 1
		}
	}
	return -1
}
