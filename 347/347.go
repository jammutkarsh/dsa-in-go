package topkfrequent

func topKFrequent(nums []int, k int) []int {
	res := make([]int, k)
	m := make(map[int]int) // nums[i]:count
	for _, v := range nums {
		m[v]++
	}
	for i := 0; i < k; i++ {
		mxCount := 0
		mxCandidate := 0
		for k, v := range m {
			if v > mxCount {
				mxCandidate = k
				mxCount = v
			}
		}
		res[i] = mxCandidate
		delete(m, mxCandidate)
	}
	return res
}
