package maximumCount

func lowerBound(arr []int, target int) int {
	low, high := 0, len(arr)-1
	i := len(arr)
	for low < high {
		mid := low + (high-low)/2
		if arr[mid] >= target {
			i = mid
			high = mid
		} else {
			low = mid + 1
		}
	}
	return i
}

func upperBound(arr []int, target int) int {
	low, high := 0, len(arr)-1
	i := len(arr)
	for low < high {
		mid := low + (high-low)/2
		if arr[mid] > target {
			i = mid
			high = mid
		} else {
			low = mid + 1
		}
	}
	return i
}

func maximumCount(nums []int) int {
	l := len(nums)
	if nums[0] > 0 || nums[l-1] < 0 { // all +ve or -ve
		return l
	}
	if nums[0] == 0 && nums[l-1] == 0 {
		return 0
	}

	neg := lowerBound(nums, 0)
	pos := l - upperBound(nums, 0)
	if pos > neg {
		return pos
	}
	return neg
}
