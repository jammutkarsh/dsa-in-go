package maximumCount

import "testing"

func TestMaximumCount(t *testing.T) {
	type args struct {
		nums []int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{"Equal Positive & Negative", args{[]int{-2, -1, -1, 1, 2, 3}}, 3},
		{"Zeros", args{[]int{0, 0, 0, 0}}, 0},
		{"Equal Positive", args{[]int{1, 1, 1, 1}}, 4},
		{"Equal Negative", args{[]int{-1, -1, -1, -1}}, 4},
		{"Positive, Negative & Zero", args{[]int{-3, -2, -1, 0, 0, 1, 2}}, 3},
		{"Positive", args{[]int{1, 2, 3, 4}}, 4},
		{"Negative", args{[]int{-1, -2, -3, -4}}, 4},
		{"LeetCode Internal", args{[]int{-1563, -236, -114, -55, 427, 447, 687, 752, 1021, 1636}}, 6},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := maximumCount(tt.args.nums); got != tt.want {
				t.Errorf("MaximumCount() = %v, want %v", got, tt.want)
			}
		})
	}
}
