package searchinsert

import "testing"

type addTest struct {
	arr      []int
	target   int
	expected int
}

var addTests = []addTest{
	{[]int{1, 3, 5, 6}, 5, 2},
	{[]int{1, 3, 5, 6}, 2, 1},
	{[]int{1, 3, 5, 6}, 7, 4},
	{[]int{1, 3, 5, 6}, 0, 0},
	{[]int{1,3, 4},2,1},
	{[]int{1},2,1},
	{[]int{},2,0},
}

func TestSearchInsert(t *testing.T) {
	for _, test := range addTests {
		output := searchInsert(test.arr, test.target); 
		if output != test.expected {
			t.Errorf("\nFor input %v\nExpected: %d \t Got: %d\n\n", test.arr, test.expected, output)
		}
	}
}
