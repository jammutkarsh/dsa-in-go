package searchinsert

func searchInsert(nums []int, target int) (mid int) {
	high, low := len(nums)-1, 0
	if len(nums) == 0 {
		return 0
	}
	for low < high {
		mid = low + (high-low)/2
		if nums[mid] == target {
			return mid
		} else if nums[mid] > target {
			high = mid
		} else {
			low = mid + 1
		}
	}
	if nums[low] < target {
		return low + 1
	}
	return low
}
