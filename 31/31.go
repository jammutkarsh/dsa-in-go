package nextpermutation

import "slices"

func nextPermutation(nums []int) {
	piviot := -1
	for i := len(nums) - 2; i >= 0; i-- {
		if nums[i] < nums[i+1] {
			piviot = i
			break
		}
	}
	if piviot == -1 {
		slices.Reverse(nums)
		return
	}
	for i := len(nums) - 1; i > piviot; i-- {
		if nums[i] > nums[piviot] {
			nums[i], nums[piviot] = nums[piviot], nums[i]
			break
		}
	}
	slices.Reverse(nums[piviot+1:])
}
