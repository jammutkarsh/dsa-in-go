package nextpermutation

import (
	"reflect"
	"testing"
)

func Test_nextPermutation(t *testing.T) {
	tests := []struct {
		name string
		nums []int
		want []int
	}{
		{
			"Test 1",
			[]int{3, 2, 1},
			[]int{1, 2, 3},
		},
		{
			"Test 2",
			[]int{1, 1, 5},
			[]int{1, 5, 1},
		},
		{
			"Test 2",
			[]int{1, 3, 2},
			[]int{2, 1, 3},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			nextPermutation(tt.nums)
			if !reflect.DeepEqual(tt.nums, tt.want) {
				t.Errorf("nextPermutation() = %v, want %v", tt.nums, tt.want)
			}
		})
	}
}
