package subtractProductAndSum

import "testing"

type addTest struct {
	num int
	expected int
}

var addTests = []addTest{
	{234, 15},
	{4421,21},
}

func TestSubtractProductAndSum(t *testing.T) {
	for _, test := range addTests{
		if output:= subtractProductAndSum(test.num); output!=test.expected{
			t.Errorf("Output: %d \t Expected: %d", output, test.expected)
		}
	}
}