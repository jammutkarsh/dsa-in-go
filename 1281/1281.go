package subtractProductAndSum

func subtractProductAndSum(n int )  int{
	product, sum := 1,0
	for n>0 {
		product= product*(n%10)
		sum= sum+(n%10)
		n/=10
	}
	return product - sum
}