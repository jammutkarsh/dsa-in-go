package answerqueries

import (
	"reflect"
	"testing"
)

func Test_answerQueries(t *testing.T) {
	type args struct {
		nums    []int
		queries []int
	}
	tests := []struct {
		name    string
		args    args
		wantAns []int
	}{
		{
			name: "example1",
			args: args{
				nums:    []int{4, 5, 2, 1},
				queries: []int{3, 10, 21},
			},
			wantAns: []int{2, 3, 4},
		},
		{
			name: "example2",
			args: args{
				nums:    []int{2, 3, 4, 5},
				queries: []int{1},
			},
			wantAns: []int{0},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotAns := answerQueries(tt.args.nums, tt.args.queries); !reflect.DeepEqual(gotAns, tt.wantAns) {
				t.Errorf("answerQueries() = %v, want %v", gotAns, tt.wantAns)
			}
		})
	}
}
