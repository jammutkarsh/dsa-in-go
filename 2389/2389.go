package answerqueries

import "sort"

// Since we are simply need to return the max len
// So, even if we rearrage the array, the total sum would remain constant.
func answerQueries(nums []int, queries []int) (ans []int) {
	sort.Ints(nums)
	for i := 1; i < len(nums); i++ {
		nums[i] += nums[i-1]
	}
	for _, v := range queries {
		ans = append(ans, bs(nums, v))
	}
	return
}

func bs(arr []int, key int) (res int) {
	if arr[0] > key {
		return 0
	}
	low, high := 0, len(arr)-1
	for low <= high {
		mid := low + (high-low)/2
		if arr[mid] <= key {
			res = mid
			low = mid + 1
		} else {
			high = mid - 1
		}
	}
	return res + 1
}
