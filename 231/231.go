package ispoweroftwo

func isPowerOfTwo(n int) bool {
	if n == 1 {
		return true
	}
	if n <= 0 || n%2 != 0 {
		return false
	}
	// needed in both.
	
	return isPowerOfTwo(n / 2)
	// OR 
	// for ; n>1; n = n / 2 {
	// 	if n%2 != 0 {
	// 		return false
	// 	}
	// }
	// return true
}
