package ispoweroftwo

import "testing"

func Test_isPowerOfTwo(t *testing.T) {
	type args struct {
		n int
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{"1", args{1}, true},
		{"2", args{2}, true},
		{"6", args{6}, false},
		{"8", args{8}, true},
		{"10", args{10}, false},
		{"12", args{12}, false},
		{"16", args{16}, true},
		{"31", args{31}, false},
		{"32", args{32}, true},
		{"126", args{126}, false},
		{"186", args{186}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isPowerOfTwo(tt.args.n); got != tt.want {
				t.Errorf("isPowerOfTwo() = %v, want %v\n\n", got, tt.want)
			}
		})
	}
}
