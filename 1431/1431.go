package kidsWithCandies

func max(nums []int) (max int) {
	for i := 0; i < len(nums); i++ {
		if nums[i] > max {
			max = nums[i]
		}
	}
	return
}

func kidsWithCandies(candies []int, extraCandies int) []bool {
	maxCandies := max(candies)
	maxOrNot := make([]bool, len(candies))
	for i := 0; i < len(candies); i++ {
		if candies[i]+extraCandies >= maxCandies {
			maxOrNot[i] = true
		}
	}
	return maxOrNot
}
