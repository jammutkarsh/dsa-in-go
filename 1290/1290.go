package getdecimalvalue

type ListNode struct {
	Val  int
	Next *ListNode
}

func getDecimalValue(head *ListNode) (decimal int) {
	if head.Next == nil {
		return head.Val
	}
	for head != nil {
		decimal = decimal * 2
		decimal = decimal + head.Val
		head = head.Next
	}

	return decimal
}
