package getdecimalvalue

import "testing"

func Test_getDecimalValue(t *testing.T) {
	type args struct {
		head *ListNode
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{"Example 1", args{&ListNode{1, &ListNode{0, &ListNode{1, nil}}}}, 5},
		{"Example 2", args{&ListNode{0, nil}}, 0},
		{"Example 3", args{&ListNode{1, nil}}, 1},
		{"Example 4", args{&ListNode{1, &ListNode{1,&ListNode{1,&ListNode{1,&ListNode{1,&ListNode{1,&ListNode{1,&ListNode{1,&ListNode{1,&ListNode{1, nil}}}}}}}}}}}, 1023},
		{"Example 5", args{&ListNode{1,&ListNode{0, &ListNode{0,&ListNode{0,&ListNode{0,&ListNode{0,&ListNode{0,&ListNode{0,&ListNode{0,&ListNode{0,&ListNode{0, nil}}}}}}}}}}}}, 1024},
		{"Example 6", args{&ListNode{1,&ListNode{0,&ListNode{0,&ListNode{1,&ListNode{0,&ListNode{0,&ListNode{1,&ListNode{1,&ListNode{1,&ListNode{0,&ListNode{0,&ListNode{0,&ListNode{0,&ListNode{0,&ListNode{0, nil}}}}}}}}}}}}}}}}, 18880},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getDecimalValue(tt.args.head); got != tt.want {
				t.Errorf("getDecimalValue() = %v, want %v", got, tt.want)
			}
		})
	}
}
