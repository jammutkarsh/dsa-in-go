package maxprofit

import "testing"

func Test_maxProfit(t *testing.T) {
	tests := []struct {
		name   string
		prices []int
		want   int
	}{
		{
			name:   "test1",
			prices: []int{7, 1, 5, 3, 6, 4},
			want:   5,
		},
		{
			name:   "test2",
			prices: []int{7, 6, 4, 3, 1},
			want:   0,
		},
		{
			name:   "test3",
			prices: []int{1, 2},
			want:   1,
		},
		{
			name:   "test4",
			prices: []int{2, 4, 1},
			want:   2,
		},
		{
			name:   "test5",
			prices: []int{1},
			want:   0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := maxProfit(tt.prices); got != tt.want {
				t.Errorf("maxProfit() = %v, want %v", got, tt.want)
			}
		})
	}
}
