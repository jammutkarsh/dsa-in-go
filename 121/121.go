package maxprofit

func maxProfit(prices []int) int {
	if len(prices) < 2 {
		return 0
	}
	mini, profit := prices[0], 0
	for _, v := range prices {
		cost := v - mini
		profit = max(cost, profit)
		mini = min(v, mini)
	}
	return profit
}
