package maximumWealth

func maximumWealth(holders [][]int) (max int) {
	
	for i := 0; i < len(holders); i++ {
		total:=0
		for j := 0; j < len(holders[i]); j++ {
			total+= holders[i][j]
		}
		if total>max {
			max=total
		}
	}
	return
}