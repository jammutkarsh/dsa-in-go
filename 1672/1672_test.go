package maximumWealth

import "testing"

type addTest struct {
        wealth        [][]int
        total int
}

var addTests = []addTest{
	{[][]int{{1, 2, 3}, {3, 2, 1}}, 6},
	{[][]int{{1, 5}, {7, 3}, {3, 5}}, 10},
	{[][]int{{2, 8, 7}, {7, 1, 3}, {1, 9, 5}}, 17},

}

func TestMaximumWealth(t *testing.T) {
	for _, test := range addTests {
		if got := maximumWealth(test.wealth); got != test.total {
			t.Errorf("maximumWealth(%v) = %d", test.wealth, got)
		}
	}
}