package findduplicates

import (
	"reflect"
	"testing"
)

func Test_findDuplicates(t *testing.T) {
	tests := []struct {
		name    string
		nums    []int
		wantRes []int
	}{
		{
			"t1",
			[]int{4, 3, 2, 7, 8, 2, 3, 1},
			[]int{2, 3},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotRes := findDuplicates(tt.nums); !reflect.DeepEqual(gotRes, tt.wantRes) {
				t.Errorf("findDuplicates() = %v, want %v", gotRes, tt.wantRes)
			}
		})
	}
}
