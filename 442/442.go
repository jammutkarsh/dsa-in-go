package findduplicates

func findDuplicates(nums []int) (res []int) {
	for i := range nums {
		idx := abs(nums[i]) - 1
		if nums[idx] < 0 {
			res = append(res, idx+1)
		}
		nums[idx] = -1 * nums[idx]
	}
	return
}

func abs(n int) int {
	if n < 0 {
		return n * -1
	}
	return n
}
