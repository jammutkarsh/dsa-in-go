package longestconsecutive

import "testing"

func Test_longestConsecutive(t *testing.T) {
	tests := []struct {
		name string
		nums []int
		want int
	}{
		{"TC 1", []int{}, 0},
		{"TC 2", []int{100, 4, 200, 2, 3, 1}, 4},
		{"TC 3", []int{0, 3, 7, 2, 5, 8, 4, 6, 0, 1}, 9},
		{"TC 4", []int{0, 300, 700, 200, 500, 800, 400, 600, 000, 100}, 1},
		{"TC 5", []int{0, 0}, 1},
		{"TC 6", []int{1, 2, 0, 1}, 3},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := longestConsecutive(tt.nums); got != tt.want {
				t.Errorf("longestConsecutive() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_longestConsecutive2(t *testing.T) {
	tests := []struct {
		name string
		nums []int
		want int
	}{
		{"TC 1", []int{}, 0},
		{"TC 2", []int{100, 4, 200, 2, 3, 1}, 4},
		{"TC 3", []int{0, 3, 7, 2, 5, 8, 4, 6, 0, 1}, 9},
		{"TC 4", []int{0, 300, 700, 200, 500, 800, 400, 600, 000, 100}, 1},
		{"TC 5", []int{0, 0}, 1},
		{"TC 6", []int{1, 2, 0, 1}, 3},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := longestConsecutive2(tt.nums); got != tt.want {
				t.Errorf("longestConsecutive() = %v, want %v", got, tt.want)
			}
		})
	}
}
