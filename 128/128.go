package longestconsecutive

import (
	"math"
	"sort"
)

func longestConsecutive(nums []int) (mxSeq int) {
	if len(nums) < 2 {
		return len(nums)
	}
	mxSeq = 1
	sort.Ints(nums)
	lastSmall, seq := math.MinInt, 0
	for _, v := range nums {
		if v-1 == lastSmall {
			seq++
			lastSmall = v
		} else if v != lastSmall {
			seq = 1
			lastSmall = v
		}
		mxSeq = max(mxSeq, seq)
	}
	return
}

func longestConsecutive2(nums []int) (mxSeq int) {
	m := make(map[int]bool)
	for _, v := range nums {
		m[v] = true
	}
	for k := range m {
		if !m[k-1] {
			cnt := 0
			x := k
			for m[x] {
				cnt++
				x++
			}
			mxSeq = max(mxSeq, cnt)
		}
	}
	return
}
