package runningSum

func runningSum(nums []int) []int {
	preSum := 0
	for i, val := range nums {
		preSum +=val
		nums[i]=preSum 
	}
	return nums
}
