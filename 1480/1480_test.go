package runningSum

import "testing"

type addTest struct {
	arr      []int
	expected []int
}

var addTests = []addTest{
	{[]int{1, 2, 3, 4}, []int{1, 3, 6, 10}},
	{[]int{1, 1, 1, 1, 1}, []int{1, 2, 3, 4, 5}},
	{[]int{3, 1, 2, 10, 1}, []int{3, 4, 6, 16, 17}},
}

func TestRunningSum(t *testing.T) {
	for _, test := range addTests {
		output := runningSum(test.arr)
		for i, v := range test.expected {
			if v != output[i] {
				t.Errorf("Output: %d \t Expected: %d", output, test.expected)
				break
			}
		}
	}
}
