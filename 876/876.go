package middlenode

type ListNode struct {
	Val  int
	Next *ListNode
}

func middleNode(head *ListNode) (middle *ListNode) {
	count := 0
	for current := head; current != nil; current = current.Next {
		count++
	}
	middle = head
	for i := 0; i < (count / 2); i++ {
		middle = middle.Next
	}
	return
}

func middleNode2(head *ListNode) *ListNode {
	slow, fast := head, head
	for fast != nil && fast.Next!=nil {
		slow = slow.Next
		fast = fast.Next.Next
	}
	return slow
}
