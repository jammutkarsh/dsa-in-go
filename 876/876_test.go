package middlenode

import (
	"reflect"
	"testing"
)

func Test_middleNode(t *testing.T) {
	type args struct {
		head *ListNode
	}
	tests := []struct {
		name       string
		args       args
		wantMiddle *ListNode
	}{
		{"Example 1", args{&ListNode{1, &ListNode{2, nil}}}, &ListNode{2, nil}},
		{"Example 2", args{&ListNode{1, &ListNode{2, &ListNode{3, &ListNode{4, &ListNode{5, nil}}}}}}, &ListNode{3, &ListNode{4, &ListNode{5, nil}}}},
		{"Example 3", args{&ListNode{1, &ListNode{2, &ListNode{3, &ListNode{4, &ListNode{5, &ListNode{6, nil}}}}}}}, &ListNode{4, &ListNode{5, &ListNode{6, nil}}}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotMiddle := middleNode(tt.args.head); !reflect.DeepEqual(gotMiddle, tt.wantMiddle) {
				t.Errorf("middleNode() = %v, want %v", gotMiddle, tt.wantMiddle)
			}
		})
	}
}
func Test_middleNode2(t *testing.T) {
	type args struct {
		head *ListNode
	}
	tests := []struct {
		name       string
		args       args
		wantMiddle *ListNode
	}{
		{"Example 1", args{&ListNode{1, &ListNode{2, nil}}}, &ListNode{2, nil}},
		{"Example 2", args{&ListNode{1, &ListNode{2, &ListNode{3, &ListNode{4, &ListNode{5, nil}}}}}}, &ListNode{3, &ListNode{4, &ListNode{5, nil}}}},
		{"Example 3", args{&ListNode{1, &ListNode{2, &ListNode{3, &ListNode{4, &ListNode{5, &ListNode{6, nil}}}}}}}, &ListNode{4, &ListNode{5, &ListNode{6, nil}}}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotMiddle := middleNode2(tt.args.head); !reflect.DeepEqual(gotMiddle, tt.wantMiddle) {
				t.Errorf("middleNode() = %v, want %v", gotMiddle, tt.wantMiddle)
			}
		})
	}
}
