package fib

import "testing"

func TestFib(t *testing.T) {
	type args struct {
		n int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{"Fib(0)", args{0}, 0},
		{"Fib(1)", args{1}, 1},
		{"Fib(4)", args{4}, 3},
		{"Fib(7)", args{7}, 13},
		
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := fib(tt.args.n); got != tt.want {
				t.Errorf("Fib() = %v, want %v", got, tt.want)
			}
		})
	}
}
