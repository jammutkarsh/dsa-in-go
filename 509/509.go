package fib

var recursionArray map[int]int
// this is an unrefferenced variable. It will be used to initialize the map.

func fib(n int) int {
	if n <= 1 {
		return n
	}
	/*Style 1: Using a for loop*/
	// t0, t1, sum := 0,1,0
	// for i := 2; i <=n; i++ {
	// 	sum = t0+t1
	// 	t0 = t1
	// 	t1= sum
	// }
	// return sum

	/*Style 2: Using recrusrion*/

	/*Style 2.1: Using simple recrusrion*/
	// return fib(n-1) + fib(n-2)

	/*Style 2.1: Using  recrusrion with a hash table array or map.*/
	if recursionArray == nil {
		recursionArray = make(map[int]int)
	}
	if _, ok := recursionArray[n]; ok { 
		return recursionArray[n]
	}
	recursionArray[n] = fib(n-1) + fib(n-2)
	return recursionArray[n]

}