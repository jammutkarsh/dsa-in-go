package minimumindex

func minimumIndex(nums []int) int {
	majE, cnt := majorityElement(nums)
	if cnt == 0 { // no element is majority
		return -1 // index
	}

	dominantCount, splitIndex := 0, -1

	// first half check
	for i, v := range nums {
		if v == majE {
			dominantCount++
		}
		if dominantCount*2 > i+1 {
			splitIndex = i
			break
		}
	}
	
	// remaining half check
	if len(nums)-(splitIndex+1) < 2*(cnt-dominantCount) {
		return splitIndex
	}
	return -1
}

func majorityElement(nums []int) (candidate int, count int) {
	for _, v := range nums {
		if count == 0 {
			candidate, count = v, 1
			continue
		}
		if v == candidate {
			count++
		} else {
			count--
		}
	}

	// Verifying the element
	count = 0
	for _, v := range nums {
		if candidate == v {
			count++
		}
	}
	if 2*count > len(nums) { // or len(nums)/2 < count
		return candidate, count
	}
	return -1, 0
}
