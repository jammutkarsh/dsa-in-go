package minimumindex

import "testing"

func Test_minimumIndex(t *testing.T) {
	tests := []struct {
		nums []int
		want int
	}{
		{[]int{1,2,2,2}, 2},
		{[]int{2,1,3,1,1,1,7,1,2,1}, 4},
		{[]int{3,3,3,3,7,2,2}, -1},

	}
	for _, tt := range tests {
		t.Run("", func(t *testing.T) {
			if got := minimumIndex(tt.nums); got != tt.want {
				t.Errorf("minimumIndex() = %v, want %v", got, tt.want)
			}
		})
	}
}
